import React from 'react';
import HeaderBlock from './Components/HeaderBlock';
import ContentBlock from './Components/ContentBlock';
import FooterBlock from './Components/FooterBlock';

const App = () =>{

  return(
  <>
    <HeaderBlock 
    title="Ну что ж это второй урок"
    descr="Выражаю свою благодарность за такие короткие и полезные ролики"/>
      
    <ContentBlock 
    title="Вот и контентик подъехал"
    descr="Возможно ли из App.js передать массив в ContentBlock, чтобы отобразить его в виде списка? Или для этого необходим Redux?"
    />
    <FooterBlock 
    title="Здесь, пожалуй, будет мой псевдоним DanyaTangens"
    />
  </>
  )
}

export default App;
