import React from 'react';
import s from './FooterBlock.module.scss';


const FooterBlock = ({title}) => {
    return(
        <div className={s.cover}>
            <div className={s.wrapper}>

                <div className={s.footer}>
                    {title && <h1>{title}</h1>}
                </div>

            </div>
        </div>
    )
}

export default FooterBlock;