import React from 'react';
import s from './ContentBlock.module.scss';

const ContentBlock = ({title, descr}) => {
    const fruits = [
        "Яблоко",
        "Апельсин",
        "Слива",
      ];
      
    const listItems = fruits.map((fruit) =>
    <li>{fruit}</li>);

    return(
        <div className={s.cover}>
    <div className={s.wrap}>
        {title && <h1 className={s.header}>{title}</h1>}
        
    {descr && <p className={s.descr}>{descr}</p>}
    <div className = {s.content}>
        <ul>{listItems}</ul>
    </div>
    </div>
</div>
    )
}

export default ContentBlock;